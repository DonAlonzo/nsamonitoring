
import se.lth.cs.camera.Axis211A;

public class CameraHandler extends Thread {
	private ServerMonitor monitor;
	private Axis211A camera;
	
	public CameraHandler(ServerMonitor monitor) {
		this.monitor = monitor;
		camera = new Axis211A();
		if (!camera.connect()) {
			System.exit(1);
		}
	}

	public void run() {
		int length;
		byte[] image = new byte[Axis211A.IMAGE_BUFFER_SIZE];
		while (true) {
			length = camera.getJPEG(image, 0);
			monitor.refreshImage(image, length);
		}
	}
}
