
import java.io.IOException;

public class CameraServer {
	public static final int CLIENT_PORT_1 = 7706;
	public static final String CAMERA_ADDRESS_1 = "argus-8.student.lth.se";
	public static final int CAMERA_PORT_1 = 7014;
	
	public static final int CLIENT_PORT_2 = 8706;
	public static final String CAMERA_ADDRESS_2 = "argus-2.student.lth.se";
	public static final int CAMERA_PORT_2 = 8014;

	public static void main(String[] args) throws IOException {
		ServerMonitor monitor1 = new ServerMonitor(CLIENT_PORT_1, CAMERA_ADDRESS_1, CAMERA_PORT_1);
		CameraHandler cameraHandler1 = new CameraHandler(monitor1);
		ImageSender imageSender1 = new ImageSender(monitor1);
		ServerConnection serverConnection1 = new ServerConnection(monitor1);
		cameraHandler1.start();
		imageSender1.start();
		serverConnection1.start();
	}

}
