
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

//import se.lth.cs.cameraproxy.Axis211A;

public abstract class Connection extends Thread {

	protected InputStream inputStream;
	protected OutputStream outputStream;

	protected Connection() {
	}

	public Message receive() throws IOException {
		if (inputStream == null || !isConnected())
			return null;
		byte[] blen = new byte[4];
		inputStream.read(blen);
		byte[] data = new byte[MessageParser.parseInt(blen, 0)];

		int bytesRead = 0;
		int bytesLeft = data.length;
		int status;

		do {
			status = inputStream.read(data, bytesRead, bytesLeft);
			if (status > 0) {
				bytesRead += status;
				bytesLeft -= status;
			}
		} while (status >= 0 && bytesLeft > 0);

		return MessageParser.parseMessage(data);
	}

	public void send(Message message) throws IOException {
		if (outputStream == null || !isConnected())
			return;
		//System.out.println("sending: " + message);
		byte[] data = MessageParser.toByteArray(message);
		byte[] send = new byte[data.length + 4];
		MessageParser.appendInt(send, 0, data.length);
		for (int i = 0; i < data.length; i++) {
			send[4 + i] = data[i];
		}
		outputStream.write(send);
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public void setOutputStream(OutputStream outputStream) {
		this.outputStream = outputStream;
	}

	public abstract boolean isConnected();

}
