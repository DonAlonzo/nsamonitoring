
public class FrameMessage extends Message {

	private byte[] data;

	public FrameMessage(long timestamp, byte[] data) {
		super(timestamp);
		this.data = data;
	}

	public byte[] getData() {
		return data;
	}

	public String toString() {
		return "FrameMessage (ts:" + timestamp + ", length:" + data.length
				+ ", crc:" + crc(data) + ")";
	}

	private static int crc(byte[] buf) {
		int crc = 0xFFFF;
		for (int pos = 0; pos < buf.length; pos++) {
			crc ^= (int) buf[pos];
			for (int i = 8; i != 0; i--) {
				if ((crc & 0x0001) != 0) {
					crc >>= 1;
					crc ^= 0xA001;
				} else
					crc >>= 1;
			}
		}
		return crc;
	}
}
