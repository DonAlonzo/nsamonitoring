
public class MessageParser {

	private static final byte TYPE_CHANGE_STATE = 1;
	private static final byte TYPE_FRAME = 2;
	private static final byte TYPE_PING = 3;
	private static final byte TYPE_PONG = 4;
	private static final byte TYPE_DISCONNECT = 5;

	public static Message parseMessage(byte[] bytes) {
		Message message = null;
		switch (bytes[0]) {
		case TYPE_CHANGE_STATE:
			message = new ChangeStateMessage(parseLong(bytes, 1), bytes[9]);
			break;
		case TYPE_FRAME:
			int length = parseInt(bytes, 9);
			byte[] data = new byte[length];
			for (int i = 0; i < length; i++)
				data[i] = bytes[13 + i];
			message = new FrameMessage(parseLong(bytes, 1), data);
			break;
		case TYPE_PING:
			message = new PingMessage(parseLong(bytes, 1));
			break;
		case TYPE_PONG:
			message = new PongMessage(parseLong(bytes, 1));
			break;
		case TYPE_DISCONNECT:
			message = new DisconnectMessage(parseLong(bytes, 1));
			break;
		}
		return message;
	}

	public static byte[] toByteArray(Message message) {
		byte[] barray = null;
		if (message instanceof FrameMessage) {
			FrameMessage frameMessage = (FrameMessage) message;
			barray = new byte[13 + frameMessage.getData().length];
			barray[0] = TYPE_FRAME;
			appendLong(barray, 1, message.getTimestamp());
			appendInt(barray, 9, frameMessage.getData().length);
			for (int i = 0; i < frameMessage.getData().length; i++) {
				barray[13 + i] = frameMessage.getData()[i];
			}
		} else if (message instanceof ChangeStateMessage) {
			ChangeStateMessage csMessage = (ChangeStateMessage) message;
			barray = new byte[10];
			barray[0] = TYPE_CHANGE_STATE;
			appendLong(barray, 1, message.getTimestamp());
			barray[9] = csMessage.getState();
		} else if (message instanceof PingMessage) {
			barray = new byte[9];
			barray[0] = TYPE_PING;
			appendLong(barray, 1, message.getTimestamp());
		} else if (message instanceof PongMessage) {
			barray = new byte[9];
			barray[0] = TYPE_PONG;
			appendLong(barray, 1, message.getTimestamp());
		} else if (message instanceof DisconnectMessage) {
			barray = new byte[9];
			barray[0] = TYPE_DISCONNECT;
			appendLong(barray, 1, message.getTimestamp());
		}
		return barray;
	}

	public static long parseLong(byte[] barray, int offset) {
		long value = 0;
		for (int i = 0; i < 8; i++) {
			value = (value << 8) + (barray[offset + i] & 0xff);
		}
		return value;
	}

	public static int parseInt(byte[] barray, int offset) {
		int value = 0;
		for (int i = 0; i < 4; i++) {
			value = (value << 8) + (barray[offset + i] & 0xff);
		}
		return value;
	}

	public static void appendLong(byte[] barray, int offset, long value) {
		for (int i = 0; i < 8; i++) {
			barray[offset + i] = (byte) (value >> (56 - i * 8));
		}
	}

	public static void appendInt(byte[] barray, int offset, int value) {
		for (int i = 0; i < 4; i++) {
			barray[offset + i] = (byte) (value >> (24 - i * 8));
		}
	}

}
