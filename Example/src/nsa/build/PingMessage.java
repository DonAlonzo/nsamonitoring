
public class PingMessage extends Message {

	public PingMessage(long timestamp) {
		super(timestamp);
	}

	public String toString() {
		return "PingMessage (ts:" + timestamp + ")";
	}

}
