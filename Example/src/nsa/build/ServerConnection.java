
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerConnection extends Connection {

	private ServerMonitor monitor;
	private ServerSocket serverSocket;
	private Socket clientSocket;

	public ServerConnection(ServerMonitor monitor) throws IOException {
		super();
		this.monitor = monitor;
		monitor.registerConnection(this);
		serverSocket = new ServerSocket(monitor.getClientPort());
	}

	public boolean isConnected() {
		return clientSocket != null ? clientSocket.isConnected() : false;
	}

	public void run() {
		while (true) {
			try {
				clientSocket = serverSocket.accept();
				setInputStream(clientSocket.getInputStream());
				setOutputStream(clientSocket.getOutputStream());
				while (isConnected()) {
					Message message = receive();
					//System.out.println("received: " + message);
					if (message instanceof PingMessage) {
						send(new PongMessage(
								((PingMessage) message).getTimestamp()));
					} else if (message instanceof DisconnectMessage) {
						clientSocket = null;
					} else if (message instanceof ChangeStateMessage) {
						monitor.changeMode(((ChangeStateMessage) message).getState());
					}
				}
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}
	}

}
