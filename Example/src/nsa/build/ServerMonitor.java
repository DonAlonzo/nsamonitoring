
import java.io.IOException;
import se.lth.cs.camera.MotionDetector;

public class ServerMonitor {

//	public static final int PORT = 7706;
	private int clientPort;
	private String cameraAddress;
	private int cameraPort;
	private static final long PERIOD_SLOW = 10;
	private byte[] image;
	private int length;
	private long sendPeriod = PERIOD_SLOW;
	private ServerConnection connection;
	private MotionDetector detector;
	private boolean hasUnsentImage, hasUnsentTestImage;
	private long lastTimeImageSent;
	private static final long IDLE_PERIOD = 5000L;
	private boolean movieMode;
	private boolean autoMode;
	private boolean newMotionDetected;

	public ServerMonitor(int clientPort, String cameraAddress, int cameraPort) throws IOException {
		this.clientPort = clientPort;
		this.cameraAddress = cameraAddress;
		this.cameraPort = cameraPort;
		detector = new MotionDetector();
		hasUnsentImage = hasUnsentTestImage = false;
		movieMode = false;
		newMotionDetected = false;
		autoMode = true;
		lastTimeImageSent = 0L;
	}
	
	public synchronized int getClientPort() {
		return clientPort;
	}
	
	public synchronized String getCameraAddress() {
		return cameraAddress;
	}
	
	public synchronized int getCameraPort() {
		return cameraPort;
	}

	public synchronized void refreshImage(byte[] image, int length) {
		this.length = length;
		this.image = image;
		hasUnsentImage = hasUnsentTestImage = true;
		if (!movieMode && autoMode) {
			newMotionDetected = movieMode = detector.detect();
		}
		notifyAll();
	}
	
	public synchronized byte[] getSnapshot() {
		return subArray(image, 0, length);
	}

	public synchronized void sendImage() {
		while (!hasUnsentImage
				|| (!movieMode && System.currentTimeMillis()
						- lastTimeImageSent < IDLE_PERIOD)) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}
		hasUnsentImage = false;
		lastTimeImageSent = System.currentTimeMillis();
		try {
			if (image == null) {
				return;
			}
			if (connection != null) {
				if (newMotionDetected) {
					connection.send(new ChangeStateMessage(System
							.currentTimeMillis(),
							ChangeStateMessage.STATE_MOVIE));
					newMotionDetected = false;
				}
				connection.send(new FrameMessage(System.currentTimeMillis(),
						subArray(image, 0, length)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private byte[] subArray(byte[] barray, int offset, int length) {
		byte[] sarray = new byte[length];
		for (int i = 0; i < length; i++)
			sarray[i] = barray[offset + i];
		return sarray;
	}

	public synchronized void registerConnection(ServerConnection connection) {
		this.connection = connection;
	}

	public synchronized void changeMode(byte state) {
		switch (state) {
		case ChangeStateMessage.STATE_IDLE:
			movieMode = false;
			autoMode = true;
			break;
		case ChangeStateMessage.STATE_MOVIE:
			movieMode = true;
			autoMode = true;
			notifyAll();
			break;
		case ChangeStateMessage.STATE_IDLE_FORCED:
			movieMode = false;
			autoMode = false;
			break;
		case ChangeStateMessage.STATE_MOVIE_FORCED:
			movieMode = true;
			autoMode = false;
			notifyAll();
			break;
		default:
			break;
		}
	}
}
