package nsa.client;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import nsa.network.*;
import nsa.server.ServerMonitor;

public class ClientConnection extends Connection {

	private ClientMonitor monitor;
	private String serverAddress;
	private int cameraID;
	private Socket client;
	private int serverPort;

	public ClientConnection(String serverAddress, int cameraID,
			ClientMonitor monitor, int serverPort) throws UnknownHostException, IOException {
		super();
		this.serverAddress = serverAddress;
		this.monitor = monitor;
		this.cameraID = cameraID;
		this.serverPort = serverPort;
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				if (client != null) {
					try {
						client.close();
						inputStream.close();
						outputStream.close();
					} catch (IOException e) {
					}
				}
			}
		});
	}

	public boolean isConnected() {
		return client != null ? client.isConnected() : false;
	}

	private void connect() throws UnknownHostException, IOException {
		client = new Socket(serverAddress, serverPort);
		setInputStream(client.getInputStream());
		setOutputStream(client.getOutputStream());
		monitor.register(cameraID, this);
		System.out.println("Client connected to " + serverAddress);
	}

	public void run() {
		while (true) {
			try {
				connect();
			} catch (UnknownHostException e) {
				System.err.println("Can't resolve hostname.");
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
			
			//set cameras to idle in the beginning
			try {
				monitor.broadcast(new ChangeStateMessage(System
						.currentTimeMillis(),
						ChangeStateMessage.STATE_IDLE));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			while (isConnected()) {
				try {
					Message message = receive();
					if (message instanceof PongMessage) {
						PongMessage pong = (PongMessage) message;
						monitor.setPing(cameraID, System.currentTimeMillis()
								- pong.getTimestamp());
					} else if (message instanceof DisconnectMessage) {
						client = null;
					} else if (message instanceof FrameMessage) {
						FrameMessage frameMessage = (FrameMessage)message;
						monitor.addNewImage(cameraID, frameMessage.getData());
					} else if (message instanceof ChangeStateMessage) {
						monitor.broadcast(message);
					}
				} catch (Exception e) {
					if (!isConnected()) {
						System.err.println("Lost connection to server.");
					} else {
						e.printStackTrace();
					}
				}
			}
		}
	}

}
