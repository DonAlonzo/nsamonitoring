package nsa.client;

import java.io.IOException;
import java.net.UnknownHostException;

import nsa.network.ChangeStateMessage;

public class ClientMain {
	public static final int ID_FOR_CAMERA_1 = 0;
	public static final int ID_FOR_CAMERA_2 = 1;

	public static void main(String[] args) {
		try {
			ClientMonitor clientMonitor = new ClientMonitor();
			GUI gui = new GUI(clientMonitor);
			
			// ClientConnection clientConnection1 = new ClientConnection(
			// "130.235.35.207", 1, clientMonitor);

			ClientConnection clientConnection1 = new ClientConnection(
					"localhost", ID_FOR_CAMERA_1, clientMonitor, 7706);

			PingThread pingThread1 = new PingThread(clientConnection1);
			clientConnection1.start();
			pingThread1.start();
			
			ClientConnection clientConnection2 = new ClientConnection(
					"localhost", ID_FOR_CAMERA_2, clientMonitor, 8706);

			PingThread pingThread2 = new PingThread(clientConnection2);
			clientConnection2.start();
			pingThread2.start();
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
