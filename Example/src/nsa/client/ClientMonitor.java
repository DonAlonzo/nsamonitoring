package nsa.client;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;

import nsa.network.ChangeStateMessage;
import nsa.network.Message;
import se.lth.cs.fakecamera.Axis211A;

public class ClientMonitor {

	private static final int SYNC_THRESH = 100;
	private static final int MAX_IMAGES_PER_SECOND = 40;
	private Map<Integer, ClientConnection> connections = new HashMap<Integer, ClientConnection>();
	private Map<Integer, Long> pings = new HashMap<Integer, Long>();
	private boolean syncMode = true;
	private boolean isSynchronized = false;
	private boolean shouldRepaint = true;
	private boolean stateMovie = false;
	private ImageBuffer[] imageBuffers;

	public ClientMonitor() {
		imageBuffers = new ImageBuffer[2];
		imageBuffers[ClientMain.ID_FOR_CAMERA_1] = new ImageBuffer(
				(int) (((double) MAX_IMAGES_PER_SECOND)
						* ((double) SYNC_THRESH) / 1000.0));
		imageBuffers[ClientMain.ID_FOR_CAMERA_2] = new ImageBuffer(
				(int) (((double) MAX_IMAGES_PER_SECOND)
						* ((double) SYNC_THRESH) / 1000.0));
	}

	public synchronized void waitForUpdate() throws InterruptedException {
		while (!shouldRepaint) {
			wait();
		}
		shouldRepaint = false;
	}

	public synchronized byte[] getImageForGUI(int cameraID) {
		if (stateMovie
				&& syncMode
				&& imageBuffers[ClientMain.ID_FOR_CAMERA_1]
						.canSynchronize(imageBuffers[ClientMain.ID_FOR_CAMERA_2])) {
			isSynchronized = true;
			if (cameraID == ClientMain.ID_FOR_CAMERA_1) {
				return imageBuffers[ClientMain.ID_FOR_CAMERA_1]
						.getImageSynchronously(imageBuffers[ClientMain.ID_FOR_CAMERA_2]);
			} else {
				return imageBuffers[ClientMain.ID_FOR_CAMERA_2]
						.getImageSynchronously(imageBuffers[ClientMain.ID_FOR_CAMERA_1]);
			}
		}
		isSynchronized = false;
		return imageBuffers[cameraID].getImageAsynchronously();
	}

	public synchronized void setSynchronizedMode(boolean syncMode) {
		this.syncMode = syncMode;
	}

	public synchronized boolean isSynchronized() {
		return isSynchronized;
	}

	public synchronized void addNewImage(int cameraID, byte[] data) {
		imageBuffers[cameraID].addImage(data);
		shouldRepaint = true;
		notifyAll();
	}

	public synchronized void register(int cameraID, ClientConnection connection) {
		connections.put(cameraID, connection);
	}

	public synchronized void broadcast(Message message) throws IOException {
		switch (((ChangeStateMessage) message).getState()) {
		case ChangeStateMessage.STATE_IDLE:
			stateMovie = false;
			break;
		case ChangeStateMessage.STATE_MOVIE:
			stateMovie = true;
			break;
		case ChangeStateMessage.STATE_IDLE_FORCED:
			stateMovie = false;
			break;
		case ChangeStateMessage.STATE_MOVIE_FORCED:
			stateMovie = true;
			break;
		default:
			break;
		}
		for (ClientConnection connection : connections.values())
			connection.send(message);
	}

	public synchronized boolean getState() {
		return stateMovie;
	}

	public synchronized long getPing(int cameraID) {
		return pings.containsKey(cameraID) ? pings.get(cameraID) : -1;
	}

	public synchronized void setPing(int cameraID, long ping) {
		pings.put(cameraID, ping);
		shouldRepaint = true;
		notifyAll();
	}
	
	public synchronized static long getTimeStamp(byte[] data) {
		return 1000L
				* ((getUnsigned(data, 25) << 24)
						+ (getUnsigned(data, 26) << 16)
						+ (getUnsigned(data, 27) << 8) + (getUnsigned(data,
							28))) + 10L * getUnsigned(data, 29);
	}

	private static long getUnsigned(byte[] data, int pos) {
		return data[pos] < 0 ? 256 + data[pos] : data[pos];
	}

	public class ImageBuffer {
		private int nbrOfImages;
		private int newestImagePosition;
		private int oldestImagePosition;
		private byte[][] images;

		public ImageBuffer(int size) {
			images = new byte[size][];
			nbrOfImages = 0;
			newestImagePosition = -1;
			oldestImagePosition = -1;
		}

		public void addImage(byte[] newImage) {
			if (nbrOfImages == 0) {
				newestImagePosition = 0;
				images[newestImagePosition] = newImage;
				oldestImagePosition = 0;
				nbrOfImages++;

			} else if (nbrOfImages < images.length) {
				newestImagePosition++;
				images[newestImagePosition] = newImage;
				nbrOfImages++;

			} else {
				newestImagePosition = incrementCounter(newestImagePosition);
				images[newestImagePosition] = newImage;
				oldestImagePosition = incrementCounter(oldestImagePosition);
			}
		}

		// should only be called if canSynchronize() is true
		public byte[] getImageSynchronously(ImageBuffer other) {
			long thisTimeStamp = getTimeStamp(this.images[this.newestImagePosition]);
			long otherTimeStamp = getTimeStamp(other.images[other.newestImagePosition]);
			if (thisTimeStamp < otherTimeStamp) {
				return this.images[this.newestImagePosition];
			} else {
				int searchPosition = this.newestImagePosition;
				while (thisTimeStamp > otherTimeStamp
						&& searchPosition != this.oldestImagePosition) {
					searchPosition = decreaseCounter(searchPosition);
					thisTimeStamp = getTimeStamp(this.images[searchPosition]);
				}
				return this.images[searchPosition];
			}
		}

		public boolean canSynchronize(ImageBuffer other) {
			if (this.newestImagePosition == -1
					|| other.newestImagePosition == -1) {
				return false;
			} else {
				long thisNewestTimeStamp = getTimeStamp(this.images[this.newestImagePosition]);
				long otherNewestTimeStamp = getTimeStamp(other.images[other.newestImagePosition]);
				if (thisNewestTimeStamp - otherNewestTimeStamp > SYNC_THRESH
						|| thisNewestTimeStamp - otherNewestTimeStamp < -SYNC_THRESH) {
					return false;
				} else if (thisNewestTimeStamp - otherNewestTimeStamp > 0) {
					long thisOldestTimeStamp = getTimeStamp(this.images[this.oldestImagePosition]);
					if (thisOldestTimeStamp - otherNewestTimeStamp > 0) {
						return false;
					} else {
						return true;
					}
				} else if (thisNewestTimeStamp - otherNewestTimeStamp < 0) {
					long otherOldestTimeStamp = getTimeStamp(other.images[other.oldestImagePosition]);
					if (thisNewestTimeStamp - otherOldestTimeStamp < 0) {
						return false;
					} else {
						return true;
					}
				} else {
					return true;
				}
			}
		}

		public byte[] getImageAsynchronously() {
			if (newestImagePosition != -1) {
				return images[newestImagePosition];
			} else {
				return null;
			}
		}

		private int incrementCounter(int counter) {
			counter++;
			if (counter == images.length) {
				counter = 0;
			}
			return counter;
		}

		private int decreaseCounter(int counter) {
			counter--;
			if (counter == -1) {
				counter = images.length - 1;
			}
			return counter;
		}
	}
}
