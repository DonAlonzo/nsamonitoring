package nsa.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import nsa.network.ChangeStateMessage;

public class GUI extends JFrame {

	private ClientMonitor monitor;
	private CameraCanvas cameraCanvas1;
	private CameraCanvas cameraCanvas2;

	public GUI(final ClientMonitor monitor) {
		this.monitor = monitor;

		JPanel cameraPanel = new JPanel();
		JPanel controlPanel = new JPanel();
		JButton autoButton = new JButton("AUTO/IDLE");
		JButton idleButton = new JButton("IDLE (FORCED)");
		JButton movieButton = new JButton("MOVIE (FORCED)");
		JButton syncButton = new JButton("SYNCHRONIZED");
		JButton asyncButton = new JButton("ASYNCHRONIZED");

		autoButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				try {
					monitor.broadcast(new ChangeStateMessage(System
							.currentTimeMillis(), ChangeStateMessage.STATE_IDLE));
				} catch (IOException e) {
					System.err.println(e.getMessage());
				}
			}
		});
		idleButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				try {
					monitor.broadcast(new ChangeStateMessage(System
							.currentTimeMillis(),
							ChangeStateMessage.STATE_IDLE_FORCED));
				} catch (IOException e) {
					System.err.println(e.getMessage());
				}
			}
		});
		movieButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				try {
					monitor.broadcast(new ChangeStateMessage(System
							.currentTimeMillis(),
							ChangeStateMessage.STATE_MOVIE_FORCED));
				} catch (IOException e) {
					System.err.println(e.getMessage());
				}
			}
		});
		syncButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				monitor.setSynchronizedMode(true);
			}
		});
		asyncButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				monitor.setSynchronizedMode(false);
			}
		});

		cameraCanvas1 = new CameraCanvas(ClientMain.ID_FOR_CAMERA_1);
		cameraCanvas2 = new CameraCanvas(ClientMain.ID_FOR_CAMERA_2);
		cameraPanel.setLayout(new GridLayout(1, 2));
		cameraPanel.add(cameraCanvas1);
		cameraPanel.add(cameraCanvas2);
		controlPanel.setLayout(new GridLayout(2, 3));
		controlPanel.add(idleButton);
		controlPanel.add(movieButton);
		controlPanel.add(autoButton);
		controlPanel.add(syncButton);
		controlPanel.add(asyncButton);
		add(cameraPanel, BorderLayout.CENTER);
		add(controlPanel, BorderLayout.SOUTH);

		/*
		 * JTextField field = new JTextField(); field.setText("ALARM!");
		 * field.setBounds(10, 10, 10, 10); add(cameraPanel,
		 * BorderLayout.NORTH);
		 */
		setSize(656, 314);
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((screen.width - getWidth()) / 2,
				(screen.height - getHeight()) / 2);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		// setExtendedState(MAXIMIZED_BOTH);
		setVisible(true);

		new Thread() {
			public void run() {
				while (true) {
					try {
						monitor.waitForUpdate();
						cameraCanvas1.refresh(monitor
								.getImageForGUI(ClientMain.ID_FOR_CAMERA_1));
						cameraCanvas2.refresh(monitor
								.getImageForGUI(ClientMain.ID_FOR_CAMERA_2));
						repaint();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}

	class CameraCanvas extends JPanel {

		private int cameraID;
		private Image image;

		public CameraCanvas(int cameraID) {
			super();
			this.cameraID = cameraID;
		}

		public void refresh(byte[] data) {
			if (data == null) {
				return;
			}
			image = getToolkit().createImage(data);
			getToolkit().prepareImage(image, -1, -1, null);
		}

		public void paintComponent(Graphics g) {
			super.paintComponent(g);

			Graphics2D g2d = (Graphics2D) g;
			g2d.setColor(Color.BLACK);
			g2d.fillRect(0, 0, getWidth(), getHeight());
			g2d.setColor(Color.GREEN);

			if (image != null)
				while (!g2d.drawImage(image, 0, 0, getWidth(), getHeight(),
						null)) {
				}
			;

			long ping = monitor.getPing(cameraID);
			if (ping == -1) {
				String txt = "No connection to camera "
						+ Integer.toString(cameraID);
				g2d.drawString(txt, (getWidth() - g2d.getFontMetrics()
						.stringWidth(txt)) / 2, (getHeight() - g2d
						.getFontMetrics().getHeight()) / 2);

			} else {
				String txt = "Camera " + cameraID + " (Ping: "
						+ Long.toString(ping) + "ms)";
				if (monitor.isSynchronized()) {
					txt = txt + " Synchronized";
				} else {
					txt = txt + " Not Synchronized";
				}
				g2d.drawString(txt, (getWidth() - g2d.getFontMetrics()
						.stringWidth(txt)) / 2, getHeight()
						- g2d.getFontMetrics().getHeight() / 2);
			}

			if (monitor.getState()) {
				String txt = "ALARM";
				g2d.setColor(Color.RED);
				g2d.setFont(new Font(txt, 20, 20));
				g2d.drawString(txt, (getWidth() - g2d.getFontMetrics()
						.stringWidth(txt)) / 2, (70 - g2d.getFontMetrics()
						.getHeight()));
			}
		}

	}

}
