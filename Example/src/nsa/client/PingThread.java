package nsa.client;

import nsa.network.PingMessage;

public class PingThread extends Thread {
	
	private ClientConnection connection;
	
	public PingThread(ClientConnection connection) {
		this.connection = connection;
	}
	
	public void run() {
		while (true) {
			try {
				Thread.sleep(1000);
				connection.send(new PingMessage(System.currentTimeMillis()));
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
		}
	}

}
