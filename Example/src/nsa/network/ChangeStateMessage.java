package nsa.network;

public class ChangeStateMessage extends Message {

	public static final byte STATE_IDLE = 0;
	public static final byte STATE_MOVIE = 1;
	public static final byte STATE_IDLE_FORCED = 2;
	public static final byte STATE_MOVIE_FORCED = 3;
	private byte state;

	public ChangeStateMessage(long timestamp, byte state) {
		super(timestamp);
		this.state = state;
	}

	public byte getState() {
		return state;
	}

	public String toString() {
		return "ChangeStateMessage (ts:" + timestamp + ", state:"
				+ (state == STATE_IDLE ? "idle" : "movie") + ")";
	}

}
