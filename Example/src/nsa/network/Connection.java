package nsa.network;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class Connection extends Thread {

	protected InputStream inputStream;
	protected OutputStream outputStream;
	protected boolean closed = false;

	protected Connection() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				try {
					send(new DisconnectMessage(System.currentTimeMillis()));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Message receive() throws IOException {
		if (closed || inputStream == null || !isConnected())
			return null;
		byte[] blen = new byte[4];
		inputStream.read(blen);
		byte[] data = new byte[MessageParser.parseInt(blen, 0)];

		int bytesRead = 0;
		int bytesLeft = data.length;
		int status;

		do {
			status = inputStream.read(data, bytesRead, bytesLeft);
			if (status > 0) {
				bytesRead += status;
				bytesLeft -= status;
			} else if (status == -1) {
				closed = true;
			}
		} while (status >= 0 && bytesLeft > 0);

		return MessageParser.parseMessage(data);
	}

	public void send(Message message) throws IOException {
		if (outputStream == null || !isConnected())
			return;
		//System.out.println("sending: " + message);
		byte[] data = MessageParser.toByteArray(message);
		byte[] send = new byte[data.length + 4];
		MessageParser.appendInt(send, 0, data.length);
		for (int i = 0; i < data.length; i++) {
			send[4 + i] = data[i];
		}
		outputStream.write(send);
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public void setOutputStream(OutputStream outputStream) {
		this.outputStream = outputStream;
	}

	public abstract boolean isConnected();

}
