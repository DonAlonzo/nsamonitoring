package nsa.network;

public class DisconnectMessage extends Message {

	public DisconnectMessage(long timestamp) {
		super(timestamp);
	}

	public String toString() {
		return "DisconnectMessage (ts:" + timestamp + ")";
	}

}
