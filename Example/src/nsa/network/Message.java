package nsa.network;

public abstract class Message {

	protected long timestamp;

	public Message(long timestamp) {
		this.timestamp = timestamp;
	}

	public long getTimestamp() {
		return timestamp;
	}

}
