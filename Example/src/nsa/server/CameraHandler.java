package nsa.server;

import se.lth.cs.cameraproxy.Axis211A;

public class CameraHandler extends Thread {
	private ServerMonitor monitor;
	private Axis211A camera;
	
	public CameraHandler(ServerMonitor monitor) {
		this.monitor = monitor;
		camera = new Axis211A(monitor.getCameraAddress(), monitor.getCameraPort());
		if (!camera.connect()) {
			System.out.println("Failed to connect to camera!");
			System.exit(1);
		}
	}

	public void run() {
		int length;
		byte[] image = new byte[Axis211A.IMAGE_BUFFER_SIZE];
		while (true) {
			length = camera.getJPEG(image, 0);
			monitor.refreshImage(image, length);
		}
	}
}
