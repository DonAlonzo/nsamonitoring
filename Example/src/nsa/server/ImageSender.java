package nsa.server;

public class ImageSender extends Thread {
	private ServerMonitor monitor;

	
	public ImageSender(ServerMonitor monitor){
		this.monitor = monitor;
	}
	
	public void run() {
		while (true) {
			monitor.sendImage();
		}
	}

}
