package nsa.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import nsa.network.*;

public class ServerConnection extends Connection {

	private ServerMonitor monitor;
	private ServerSocket serverSocket;
	private Socket clientSocket;

	public ServerConnection(ServerMonitor monitor) throws IOException {
		super();
		this.monitor = monitor;
		monitor.registerConnection(this);
		serverSocket = new ServerSocket(monitor.getClientPort());
		System.out.println("CameraServer operating at port "
				+ monitor.getClientPort() + " started.");
	}

	public boolean isConnected() {
		return clientSocket != null ? !closed : false;
	}

	public void run() {
		while (true) {
			try {
				clientSocket = serverSocket.accept();
				closed = false;
				setInputStream(clientSocket.getInputStream());
				setOutputStream(clientSocket.getOutputStream());
				while (isConnected()) {
					Message message = receive();
					if (message instanceof PingMessage) {
						send(new PongMessage(
								((PingMessage) message).getTimestamp()));
					} else if (message instanceof DisconnectMessage) {
						clientSocket = null;
					} else if (message instanceof ChangeStateMessage) {
						monitor.changeMode(((ChangeStateMessage) message).getState());
					}
				}
			} catch (IOException e) {
				System.err.println("hehuehue");
			}
		}
	}

}
